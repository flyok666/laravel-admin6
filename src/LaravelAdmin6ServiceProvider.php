<?php

namespace felixji\LaravelAdmin6;

use Illuminate\Support\ServiceProvider;

class LaravelAdmin6ServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        include __DIR__.'/routes.php';
//        $this->app->make('Controller');//TODO!
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //$this->loadViewsFrom(__DIR__.'/resources/view','xxx');//TODO!
    }
}
